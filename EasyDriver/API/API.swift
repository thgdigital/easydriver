//
//  API.swift
//  EasyDriver
//
//  Created by Thiago Santos on 10/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON



class Api {
    static let sharedInstance = Api()
    
    func getHistoricos(url: String, completionHandler: @escaping (_ result: ([Historico]), _  error: Error?) -> ()){
        Alamofire.request(url).validate().responseJSON {  response in
        
            switch response.result {
                case .success:
              
                var  historicos = [Historico]()
                
                if let jsonObject = response.result.value {
                    let json = JSON(jsonObject)
                    for (_, subJson):(String, JSON) in json{
                        
                        let hist = Historico(json: subJson)
                        historicos.append(hist)
                    
                  
                }
                    let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                        completionHandler(historicos, nil)
                    })
            }
              case let .failure(error):
                    completionHandler([], error)
                }
            }
        }
    func getPedidos(url: String, completionHandler: @escaping (_ result: ([Pedido]), _  error: Error?) -> ()){
        Alamofire.request(url).validate().responseJSON {  response in
            
            switch response.result {
            case .success:
                
                var  pedidos = [Pedido]()
                
                if let jsonObject = response.result.value {
                    let json = JSON(jsonObject)
                    for (_, subJson):(String, JSON) in json{
                        
                        let hist = Pedido(json: subJson)
                        pedidos.append(hist)
                        
                        
                    }
                    let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                        completionHandler(pedidos, nil)
                    })
                }
            case let .failure(error):
                completionHandler([], error)
            }
        }
    }
    func getFavoritos(url: String, completionHandler: @escaping (_ result: ([Favorito]), _  error: Error?) -> ()){
        Alamofire.request(url).validate().responseJSON {  response in
            
            switch response.result {
            case .success:
                
                var  favoritos = [Favorito]()
                
                if let jsonObject = response.result.value {
                    let json = JSON(jsonObject)
                    for (_, subJson):(String, JSON) in json{
                        
                        let fav = Favorito(json: subJson)
                        favoritos.append(fav)
                        
                        
                    }
                    let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                        completionHandler(favoritos, nil)
                    })
                }
            case let .failure(error):
                completionHandler([], error)
            }
        }
    }

}
