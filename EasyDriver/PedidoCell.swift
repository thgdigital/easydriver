//
//  PedidoCell.swift
//  EasyDriver
//
//  Created by Thiago Santos on 11/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import SDWebImage


class PedidoCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var reaisLabel: UILabel!
    
    var pedido : Pedido?{
        didSet{
            if let status = pedido?.status{
                reaisLabel.text = status
            }
            if let motorista = pedido?.motorista?.nome {
                tituloLabel.text = motorista
            }
            if let carro = pedido?.motorista?.carro{
                subTitleLabel.text = carro
            }
            if let image = pedido?.motorista?.image {
                avatarImageView.sd_setImage(with: URL(string: image))
            }
           
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatarImageView?.layer.cornerRadius = avatarImageView.frame.height / 2
        avatarImageView?.contentMode = .scaleAspectFill
        avatarImageView?.clipsToBounds = true
    }

}
