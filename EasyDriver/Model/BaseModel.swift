//
//  BaseModel.swift
//  EasyDriver
//
//  Created by Thiago Santos on 10/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

//import UIKit
//import ObjectMapper
//
//public class BaseModel : Mappable {
//    
//    // MARK: Initializers
//    public init() {}
//    public required init?(map: Map){}
//    
//    // MARK: Mappable Protocol
//    public func mapping(map: Map) {}
//    
//    // MARK: - Date Transform
//    public class func dateTransform() -> DateFormatterTransform {
//        return self.dateTransform(format: "yyyy-MM-dd'T'HH:mm:ss'Z'")
//    }
//    
//    final public class func dateTransform(format: String) -> DateFormatterTransform {
//        return CustomDateFormatTransform(formatString: format)
//    }
//}
