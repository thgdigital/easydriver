//
//  FIRImage.swift
//  EasyDriver
//
//  Created by Thiago Santos on 26/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage

class FIRImage {
    
    var image: UIImage
    var downloadUrl: URL?
    var downloadUrlString: String!
    var ref: StorageReference!
    
    init(image: UIImage) {
        self.image = image
    }
    func salveProfileImage(_ userUED: String, _ completion: @escaping(Error?) -> Void){
        let imageResized = image.resized()
        if let imageData = UIImageJPEGRepresentation(imageResized, 0.9){
            ref = StorageReferencia.profileImage.referencia().child(userUED)
            downloadUrlString = ref.description
            
            ref.putData(imageData, metadata: nil, completion: { (data, error) in
                completion(error)
                
//                guard let error = error else {
//                    completion(error)
//                    return
//                }
                
            })
        }
    }
    
}
private extension UIImage{
    
    func resized() -> UIImage {
        let height: CGFloat = 1000.0
        let ratio           = self.size.width / self.size.height
        let width           = height * ratio
        let newSize         = CGSize(width: width, height: height)
        let newRetangulo    = CGRect(x: 0, y: 0, width: width, height: height)
        UIGraphicsBeginImageContext(newSize)
        self.draw(in: newRetangulo)
        
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        
            UIGraphicsEndImageContext()
        
        return resizedImage!
    }
}
