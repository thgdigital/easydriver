//
//  StorageReferencia.swift
//  EasyDriver
//
//  Created by Thiago Santos on 26/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import FirebaseStorage


enum StorageReferencia {
    case root
    case profileImage
    
    func referencia() -> StorageReference {
        switch self {
        case .root:
            return rootRef
        default:
            return rootRef.child(path)
        }
    }
    
    private var rootRef: StorageReference {
        return Storage.storage().reference()
    }
    
    private var path: String {
        switch self {
        case .root:
            return ""
        case .profileImage:
            return "profileImage"
        }
        
    }
}
