//
//  Motorista.swift
//  EasyDriver
//
//  Created by Thiago Santos on 10/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Motorista{
    
    var id:     Int?
    var nome:   String?
    var image:  String?
    var carro:  String?
    
    init() {}
    
    init(json: JSON) {
        
        id      = json["id"].intValue
        image   = json["imagem"].stringValue
        nome    = json["nome"].stringValue
        carro    = json["carro"].stringValue
    }

}
