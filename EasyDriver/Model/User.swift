//
//  User.swift
//  EasyDriver
//
//  Created by Thiago Santos on 26/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import UIKit
import Firebase

struct User {
    var userName: String
    let uid: String
    var fullName: String
    var profileImage: UIImage?
    var email: String
    
    init(uid: String, fullName: String, profileImage: UIImage?, email: String, userName: String) {
        self.uid = uid
        self.fullName = fullName
        self.profileImage = profileImage
        self.email = email
        self.userName = userName
        
    }
    
    init(userName: String, profileImage: UIImage?) {
        self.profileImage = profileImage
        self.userName = userName
        self.uid = ""
        self.fullName = ""
        self.email = ""
    }
    
    func save(completion: @escaping(Error?) -> Void){
        
        let ref = DatabaseReferencia.users(uid: uid).referencia()
        ref.setValue(toDicionario())
        if let profileImage = self.profileImage {
            let firImage = FIRImage(image: profileImage)
            
            firImage.salveProfileImage(uid, { (error) in
                completion(error)
            })
        
        }
    }
    
    func toDicionario() -> [String: Any] {
        
     return [
            "uid": self.uid,
            "userName": self.userName,
            "fullName": self.fullName,
            "email":self.email
        ]
    }
}
