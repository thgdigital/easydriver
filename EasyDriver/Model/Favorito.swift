//
//  Favorito.swift
//  EasyDriver
//
//  Created by Thiago Santos on 11/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Favorito {
    var id:             Int?
    var status:         String?
    var motorista:      Motorista?
    init() {}
    
    init(json: JSON){
        id                  = json["id"].intValue
        status              = json["status"].stringValue
        motorista           = Motorista(json: json["motoristas"])
    }
}
