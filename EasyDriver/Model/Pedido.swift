//
//  Pedidos.swift
//  EasyDriver
//
//  Created by Thiago Santos on 11/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import SwiftyJSON


struct Pedido{
    
    var id:             Int?
    var date_corrida:   String?
    var valor:          String?
    var status:         String?
    var motorista :     Motorista?
    
    init() {}
    
    init(json: JSON){
        
        id                  = json["id"].intValue
        date_corrida        = json["date_corrida"].stringValue
        valor               = json["valor"].stringValue
        status              = json["status"].stringValue
        motorista           = Motorista(json: json["motoristas"])
        
    }
}
