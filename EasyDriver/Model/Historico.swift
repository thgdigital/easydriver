//
//  Historico.swift
//  EasyDriver
//
//  Created by Thiago Santos on 10/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Historico{
    
    var id:             Int?
    var date_corrida:   String?
    var valor:          String?
    var motorista :     Motorista?
    
    init() {}
    
    init(json: JSON){
        
        id                  = json["id"].intValue
        date_corrida        = json["date_corrida"].stringValue
        valor               = json["valor"].stringValue
        motorista           = Motorista(json: json["motoristas"])
        
    }
}
