//
//  DatabaseReferencia.swift
//  EasyDriver
//
//  Created by Thiago Santos on 26/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import Firebase



enum DatabaseReferencia {
    case root
    case users(uid: String)
    
    func referencia() -> DatabaseReference {
        switch self {
        case .root:
            return rootRef
        default:
            return rootRef.child(path)
        }
    }
    private var rootRef: DatabaseReference{
        return Database.database().reference()
    }
    
    private var path: String {
        switch self {
            case .root:
                return ""
            case .users(let uid):
                return "users/\(uid)"
        }
      
    }
}


