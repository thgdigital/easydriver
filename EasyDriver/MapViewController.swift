//
//  ViewController.swift
//  EasyDriver
//
//  Created by Thiago Santos on 05/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit


class MapViewController: UIViewController {
   
  
    @IBOutlet weak var userLocationTextField: UITextField!
    @IBOutlet weak var destinoTextField: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var nextButton: UIButton!
    var activeTextField:  UITextField?
    let regionRadius: CLLocationDistance = 1000
    let driverCell = "driverCell"
    var calendarView: CalendarView?
    let viewSub = UIView()
    let blackView = UIView()
    var defualt = Date()
    var drivesImage = ["basic", "driver","vip-luxo","bag-suv"]
    var drivesTitle = ["Basic", "Driver", "Vip","Family"]
    let selectedIndexPath = IndexPath(item: 2, section: 0)
    let backgroudHeader: UIView = {
        let view = UIView()
        view.backgroundColor = App.color
        return view
    }()
    let formate: DateFormatter = {
        let formato = DateFormatter()
        formato.dateFormat = "MMMM , dd YYYY  HH:mm:ss"
        return formato
    }()
   lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.text = self.formate.string(from: Date()).uppercased()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    lazy var collectionViewDriver: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    let separador: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 0.5)
        return view
    }()
    let estimaButton: UIButton = {
        
        
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
       
        button.addTarget(self, action: #selector(MapViewController.estimarTarifa) , for: UIControlEvents.touchUpInside)
        button.setTitle("Estimar valor".uppercased() , for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = App.color
        return button
    }()
    
   
    
    lazy var calendarButton: UIButton = {
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        button.addTarget(self, action: #selector(MapViewController.calendarCorrida(_:)) , for: UIControlEvents.touchUpInside)
        button.setImage(UIImage(named: "calendar"), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.borderColor = UIColor.gray.cgColor
        button.layer.borderWidth = 0.5
        button.layer.cornerRadius = 5
        return button
    }()
    var calenderView: CalendarView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        titleLabel(string: "Novo Pedido")
        leftBar()
        UserLocationManager.shared.addObserver(delegate: self)
         requestUserLocation()
        userLocationTextField.delegate = self
        
        destinoTextField.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MapViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        nextButton.layer.cornerRadius = 5
        collectionViewDriver.register(DriverCell.self, forCellWithReuseIdentifier: driverCell)
        collectionViewDriver.selectItem(at: selectedIndexPath, animated: true, scrollPosition: .centeredHorizontally)
        
        if let location = UserLocationManager.shared.lastLocation {
            
            let initialLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
            
            centerMapOnLocation(location: initialLocation)
            
            self.activeTextField = userLocationTextField
           UserLocationManager.shared.geoCodes(location: initialLocation , textFielld: userLocationTextField)
        
        }
       
        mapView.delegate = self
        
       
    }
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    private func showCalendar(){
        if let window = UIApplication.shared.keyWindow {
        
            if let agendarCalendar = Bundle.main.loadNibNamed("Calendar", owner: self, options: nil)?.first as?  CalendarView {
                calendarView = agendarCalendar
                blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
                
                blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismissDate)))
                calenderView = agendarCalendar
                window.addSubview(blackView)
                
                window.addSubview(agendarCalendar)
                agendarCalendar.actionDate.layer.cornerRadius = 5
                agendarCalendar.actionDate.addTarget(self, action: #selector(MapViewController.changeDatePicker(_:)), for: .touchUpInside)
                agendarCalendar.titleLabel.text = formate.string(from: Date()).uppercased()
                agendarCalendar.calendaPicker.addTarget(self, action:  #selector(MapViewController.changeDatePicker(_:)), for: .touchUpInside)
                agendarCalendar.cancelButton.addTarget(self, action: #selector(handleDismissDate), for: .touchUpInside)
                agendarCalendar.calendaPicker.minimumDate = Date()
                
                let height: CGFloat = 340
                let y = window.frame.height - height
                agendarCalendar.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width , height: height)
                
                blackView.frame = window.frame
                blackView.alpha = 0
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    
                    self.blackView.alpha = 1
                    
                    agendarCalendar.frame = CGRect(x: 0, y: y, width: agendarCalendar.frame.width, height: agendarCalendar.frame.height)
                    
                }, completion: nil)
            }
        }
    }
    
    func changeDatePicker(_ sender: UIButton) {
        guard let cardDatePicker = calendarView else {
            handleDismissDate()
            return
        }
        self.defualt = cardDatePicker.calendaPicker.date
        dateLabel.text = formate.string(from: cardDatePicker.calendaPicker.date)
        handleDismissDate()
    }
    
    
    // MARK: - Drives
    
    
    func showDrivers() {
        //show menu
        
        if let window = UIApplication.shared.keyWindow {
            
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss(_:))))
            
            window.addSubview(blackView)
            
            window.addSubview(viewSub)
            
            viewSub.frame = CGRect(x: 0, y: self.view.frame.size.height - 230, width: view.frame.width, height: 230)
            dateLabel.text = formate.string(from: defualt).uppercased()
            viewSub.addSubview(backgroudHeader)
            backgroudHeader.addSubview(dateLabel)
            viewSub.addSubview(collectionViewDriver)
            viewSub.addSubview(separador)
            viewSub.addSubview(estimaButton)
            viewSub.addSubview(calendarButton)
            
            dateLabel.anchor(backgroudHeader.topAnchor, left: backgroudHeader.leftAnchor, bottom: backgroudHeader.bottomAnchor, right: backgroudHeader.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
            
            backgroudHeader.anchor(viewSub.topAnchor, left: viewSub.leftAnchor, bottom: collectionViewDriver.topAnchor, right: viewSub.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 50)
            
            collectionViewDriver.anchor(backgroudHeader.bottomAnchor, left: viewSub.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: self.view.frame.width , heightConstant: 90)
            viewSub.backgroundColor = .white
            separador.anchor(collectionViewDriver.bottomAnchor, left: viewSub.leftAnchor, bottom: nil, right: viewSub.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0.5)
            
            estimaButton.anchor(separador.bottomAnchor, left: collectionViewDriver.leftAnchor, bottom: nil, right: calendarButton.leftAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 50)
            
            calendarButton.anchor(estimaButton.topAnchor, left: estimaButton.rightAnchor, bottom: nil, right: viewSub.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 10, widthConstant: 50, heightConstant: 50)
            
            let height: CGFloat = 230
            let y = window.frame.height - height
            viewSub.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
            
            blackView.frame = window.frame
            blackView.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 1
                
                self.viewSub.frame = CGRect(x: 0, y: y, width: self.viewSub.frame.width, height: self.viewSub.frame.height)
                
            }, completion: nil)
        }
    }
    
    @objc func handleDismiss(_ showAgenda: Bool = false) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 0
            
            if let window = UIApplication.shared.keyWindow {
                self.viewSub.frame = CGRect(x: 0, y: window.frame.height, width: self.viewSub.frame.width, height: self.viewSub.frame.height)
            }
            
        }) { (completed: Bool) in
            
            guard showAgenda else { return }
            
            self.showCalendar()
        }
    }
    
    func handleDismissDate() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 0
            
            if let window = UIApplication.shared.keyWindow {
                if let viewCalender = self.calenderView {
                    viewCalender.frame = CGRect(x: 20, y: window.frame.height, width: viewCalender.frame.width, height: viewCalender.frame.height)
                    
                }
            }
            
        }) { (completed: Bool) in
            self.showDrivers()
        }
    }

    
    
    
   // MARK: - Actions
    @objc private func estimarTarifa(){
//        dateLabel.text = self.calendarView?.calendaPicker
    
    }
    @objc private func calendarCorrida(_ sender: UIButton){
        handleDismiss(true)
    }
    @IBAction func nextDrives(_ sender: Any) {
        guard userLocationTextField.text != "", destinoTextField.text != "" else{
            UIAlertController.showAlert(title: "Dados em branco", message: "Existem dados em brancos por favor verificar", cancelButtonTitle: "Fechar", confirmationButtonTitle: nil, viewController: nil, cancelBlock: nil)
            return
        }
        
        showDrivers()
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    
    
    func requestUserLocation() {
        
        let status = CLLocationManager.authorizationStatus()
        
        if status == .notDetermined {
            
            UserLocationManager.shared.requestAuthorization()
            
        } else if  status != .authorizedWhenInUse{
            
            UIAlertController.showAlert(
                title: "Serviço de Localização desabilitado",
                message: "Para habilitar, vá para Configurações e ative o Serviço de Localização deste aplicativo.",
                cancelButtonTitle: "Cancelar",
                confirmationButtonTitle: "Configurações",
                viewController: self,
                dissmissBlock: {
                    UserLocationManager.openSystemSettings()
            },
                cancelBlock: {
                    //self.mode = .AdjustLater
            }
            )
        }

    }
    
    
    // MARK: - Instance
    
    class func instantiateFromStoryboard() -> MapViewController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
    }

}
extension MapViewController: UserLocationManagerDelegate {
    func userLocation(manager: UserLocationManager, didChangeLocationAuthorization granted: Bool) {
//        updateLocationStatus()
    }
}
extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        let location = CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude)
        guard let textfield = activeTextField else { return }
        UserLocationManager.shared.geoCodes(location: location, textFielld: textfield)
    }
    
    
}
extension MapViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return drivesImage.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width / 4, height: 90)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionViewDriver.dequeueReusableCell(withReuseIdentifier: driverCell, for: indexPath) as! DriverCell
        
        cell.imageView.image = UIImage(named: drivesImage[indexPath.row])
        cell.titleLabel.text = drivesTitle[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    
}
// MARK: - Instance
extension MapViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {

        textField.resignFirstResponder()
        return true
    }
}
