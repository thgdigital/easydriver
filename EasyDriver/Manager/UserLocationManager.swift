//
//  UserLocationManager.swift
//  EasyDriver
//
//  Created by Thiago Santos on 12/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import CoreLocation


@objc protocol UserLocationManagerDelegate: class {
    func userLocation(manager: UserLocationManager, didChangeLocationAuthorization granted: Bool)
    @objc optional func didInferredUserLocation(manager: UserLocationManager)
}

class UserLocationManager: NSObject {
    
    // MARK: - Properties
    
    static let shared = UserLocationManager()
    
     let delegates = MulticastDelegate<UserLocationManagerDelegate>()
    
    private var locationManager: CLLocationManager!
    
    var geoCode = CLGeocoder()
  
    
    var permissionLocationGranted: Bool {
        return CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() == .authorizedWhenInUse
    }
    // Inferir a localização do usuário (Quando consegue encontrar o GPS)
     var inferredLocation = false
    
    
    
    var lastLocation: LocationCoordinate? {
        
        guard permissionLocationGranted else { return nil }
        
        guard let coordinate =  locationManager?.location?.coordinate else {
            return nil
        }
        
        return LocationCoordinate(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
    
    
    func geoCodes(location : CLLocation!, textFielld: UITextField? ) {
        /* Only one reverse geocoding can be in progress at a time hence we need to cancel existing
         one if we are getting location updates */
        textFielld?.text = "  Carregando..... "
        textFielld?.backgroundColor = UIColor(white: 0, alpha: 0.3)
        geoCode.cancelGeocode()
        geoCode.reverseGeocodeLocation(location, completionHandler: { (data, error) -> Void in
            
            guard let  field = textFielld else {
                
                return
            }
            
            guard let placeMarks = data as [CLPlacemark]! else {
                guard let erro = error else{ return }
                field.text = ""
                textFielld?.backgroundColor = .white
                UIAlertController.showAlert(title: "Opss error", message: erro.localizedDescription, cancelButtonTitle: "Fechar", cancelBlock: nil)
                return
            }
            
           
            let loc: CLPlacemark = placeMarks[0]
            let addressDict : [NSString:NSObject] = loc.addressDictionary as! [NSString: NSObject]
            let addrList = addressDict["FormattedAddressLines"] as! [String]
            let address = addrList.joined(separator: ", ")
            field.text = address
            textFielld?.backgroundColor = .white
          
            textFielld?.resignFirstResponder()

        })
        
    }
    
  
    
    
    // MARK: - Initializers
    
    private override init() {
        super.init()
        
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if permissionLocationGranted {
            locationManager.startUpdatingLocation() // Start GPS
        }
    }
    
    // MARK: - User Location
    
    func requestAuthorization() {
        locationManager.requestWhenInUseAuthorization()
    }
    
 
    
    static func openSystemSettings() {
        if let url = URL(string: UIApplicationOpenSettingsURLString) {
            UIApplication.shared.openURL(url)
        }
    }

}

// MARK: - Multicast Delegate

extension UserLocationManager {
    
    func addObserver(delegate: UserLocationManagerDelegate) {
        delegates.addDelegate(delegate: delegate)
    }
    
    func removeObserver(delegate: UserLocationManagerDelegate) {
        delegates.removeDelegate(delegate: delegate)
    }
    
    
    // MARK: Helper
    
     func invokeDelegateLocationAuthorization() {
        self.inferredLocation = false
        delegates.invoke(invocation: {
            $0.userLocation(manager: self, didChangeLocationAuthorization: self.permissionLocationGranted)
        })
    }
    
     func invokeDelegateInferredLocation() {
        delegates.invoke(invocation: {
            $0.didInferredUserLocation?(manager: self)
        })
    }
}

// MARK: - CLLocationManagerDelegate

extension UserLocationManager: CLLocationManagerDelegate {
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        invokeDelegateLocationAuthorization()
        
        switch status {
        case .notDetermined, .restricted, .denied:
            break
        default:
            manager.startUpdatingLocation()
        }
    }
    
    @objc func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       invokeDelegateLocationAuthorization()
    }
    
    @objc func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        if !inferredLocation {
            self.inferredLocation = true
            invokeDelegateInferredLocation()
        }
    
    }
}


public struct LocationCoordinate {
    
    public private(set) var latitude: Double   = 0.0
    public private(set) var longitude: Double  = 0.0
    
    public init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
}
