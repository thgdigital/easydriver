//
//  MulticastDelegate.swift
//  EasyDriver
//
//  Created by Thiago Santos on 12/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation

class MulticastDelegate <T> {
    private var weakDelegates = NSHashTable<AnyObject>.weakObjects()
    
    
    func addDelegate(delegate: T) {
        weakDelegates.add(delegate as? AnyObject)
    }
    
    func removeDelegate(delegate: T) {
        weakDelegates.remove(delegate as? AnyObject)
    }
    
    func invoke(invocation: (T) -> ()) {
        
        for delegate in weakDelegates.allObjects {
            invocation(delegate as! T)
        }
    }
}

func += <T: AnyObject> (left: MulticastDelegate<T>, right: T) {
    left.addDelegate(delegate: right)
}

func -= <T: AnyObject> (left: MulticastDelegate<T>, right: T) {
    left.removeDelegate(delegate: right)
}
