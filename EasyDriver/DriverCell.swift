//
//  DriverCell.swift
//  EasyDriver
//
//  Created by Thiago Santos on 12/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class DriverCell: BaseCollectionCell {
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "vip-luxo")
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.backgroundColor = .clear
        iv.isUserInteractionEnabled = false
        return iv
    }()
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Teste"
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textAlignment = .center
        return label
    }()
    override var isHighlighted: Bool {
        didSet {
            titleLabel.textColor = isSelected ? App.color :  .black
        }
    }
    
    override var isSelected: Bool {
        didSet {
            titleLabel.textColor = isSelected ? App.color :  .black
        }
    }

    
    override func setupViews() {
         super.setupViews()
        addSubview(imageView)
        addSubview(titleLabel)
        
        backgroundColor = .white
        imageView.anchor(topAnchor, left: leftAnchor, bottom: titleLabel.bottomAnchor, right: nil, topConstant: 0, leftConstant:0, bottomConstant: 0, rightConstant: 0, widthConstant: 80, heightConstant: 50)
        
        titleLabel.anchor(imageView.bottomAnchor, left: imageView.leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 80, heightConstant: 20)
        
       
    }

}
