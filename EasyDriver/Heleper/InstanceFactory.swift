//
//  InstanceFactory.swift
//  EasyDriver
//
//  Created by Thiago Santos on 12/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

struct InstanceFactory {
    
    static let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    static let navigationController = InstanceFactory.storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
    static let pedido = PedidoTableViewController.instantiateFromStoryboard()
    static let login = LoginController.instantiateFromStoryboard()
}

