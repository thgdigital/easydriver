//
//  Device.swift
//  EasyDriver
//
//  Created by Thiago Santos on 19/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//


import Foundation
import UIKit

extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}

// MARK: - Device Structure

public struct Device {
    
    
    // MARK: - UIDevice
    
    public  static var currentDevice: UIDevice {
        struct Singleton {
            static let device = UIDevice.current
        }
        return Singleton.device
    }
    
    public  static var currentDeviceVersion: Float {
        struct Singleton {
            static let version = UIDevice.current.systemVersion.floatValue
        }
        return Singleton.version
    }
    
    public  static var currentDeviceHeight: CGFloat {
        return UIScreen.main.bounds.size.height
    }
    
    public  static var currentDeviceWidth: CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    public static var orientation: UIDeviceOrientation {
        return UIDevice.current.orientation
    }
    
    public static func isPortraitOrientation() -> Bool {
        let orientation: UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
        return UIInterfaceOrientationIsPortrait(orientation)
    }
    
    public static func isLandscapeOrientation() -> Bool {
        return !self.isPortraitOrientation()
    }
    
    // MARK: - Device Idiom Checks
    
    public  static func isPhone() -> Bool {
        return currentDevice.userInterfaceIdiom == .phone
    }
    
    public static func isPad() -> Bool {
        return currentDevice.userInterfaceIdiom == .pad
    }
    
    // MARK: - Device Version Checks
    
    public  enum Versions: Float {
        case Five = 5.0
        case Six = 6.0
        case Seven = 7.0
        case Eight = 8.0
        case Nine = 9.0
        case Ten = 10.0
    }
    
    public  static func isVersion(version: Versions) -> Bool {
        return currentDeviceVersion >= version.rawValue && currentDeviceVersion < (version.rawValue + 1.0)
    }
    
    public static func isVersionOrLater(version: Versions) -> Bool {
        return currentDeviceVersion >= version.rawValue
    }
    
    public  static func isVersionOrEarlier(version: Versions) -> Bool {
        return currentDeviceVersion < (version.rawValue + 1.0)
    }
    
    // MARK: iOS 5 Checks
    
    public static func IS_OS_5() -> Bool {
        return isVersion(version: .Five)
    }
    
    public static func IS_OS_5_OR_LATER() -> Bool {
        return isVersionOrLater(version: .Five)
    }
    
    public static func IS_OS_5_OR_EARLIER() -> Bool {
        return isVersionOrEarlier(version: .Five)
    }
    
    // MARK: iOS 6 Checks
    
    public static func IS_OS_6() -> Bool {
        return isVersion(version: .Six)
    }
    
    public static func IS_OS_6_OR_LATER() -> Bool {
        return isVersionOrLater(version: .Six)
    }
    
    public static func IS_OS_6_OR_EARLIER() -> Bool {
        return isVersionOrEarlier(version: .Six)
    }
    
    // MARK: iOS 7 Checks
    
    public static func IS_OS_7() -> Bool {
        return isVersion(version: .Seven)
    }
    
    public static func IS_OS_7_OR_LATER() -> Bool {
        return isVersionOrLater(version: .Seven)
    }
    
    public  static func IS_OS_7_OR_EARLIER() -> Bool {
        return isVersionOrEarlier(version: .Seven)
    }
    
    // MARK: iOS 8 Checks
    
    public static func IS_OS_8() -> Bool {
        return isVersion(version: .Eight)
    }
    
    public static func IS_OS_8_OR_LATER() -> Bool {
        return isVersionOrLater(version: .Eight)
    }
    
    public static func IS_OS_8_OR_EARLIER() -> Bool {
        return isVersionOrEarlier(version: .Eight)
    }
    
    // MARK: - Device Size Checks
    
    public  enum Heights: CGFloat {
        case Inches_3_5 = 480
        case Inches_4 = 568
        case Inches_4_7 = 667
        case Inches_5_5 = 736
    }
    
    public  static func isSize(height: Heights) -> Bool {
        return currentDeviceHeight == height.rawValue
    }
    
    public static func isSizeOrLarger(height: Heights) -> Bool {
        return currentDeviceHeight >= height.rawValue
    }
    
    public  static func isSizeOrSmaller(height: Heights) -> Bool {
        return currentDeviceHeight <= height.rawValue
    }
    
    // MARK: Retina Check
    
    public  static func IS_RETINA() -> Bool {
        return (UIScreen.main.scale > 1.0)
    }
    
    // MARK: 3.5 Inch Checks
    
    public static func IS_3_5_INCHES() -> Bool {
        return isPhone() && isSize(height: .Inches_3_5)
    }
    
    public static func IS_3_5_INCHES_OR_LARGER() -> Bool {
        return isPhone() && isSizeOrLarger(height: .Inches_3_5)
    }
    
    public static func IS_3_5_INCHES_OR_SMALLER() -> Bool {
        return isPhone() && isSizeOrSmaller(height: .Inches_3_5)
    }
    
    // MARK: 4 Inch Checks
    
    public static func IS_4_INCHES() -> Bool {
        return isPhone() && isSize(height: .Inches_4)
    }
    
    public static func IS_4_INCHES_OR_LARGER() -> Bool {
        return isPhone() && isSizeOrLarger(height: .Inches_4)
    }
    
    public static func IS_4_INCHES_OR_SMALLER() -> Bool {
        return isPhone() && isSizeOrSmaller(height: .Inches_4)
    }
    
    // MARK: 4.7 Inch Checks
    
    public  static func IS_4_7_INCHES() -> Bool {
        return isPhone() && isSize(height: .Inches_4_7)
    }
    
    public  static func IS_4_7_INCHES_OR_LARGER() -> Bool {
        return isPhone() && isSizeOrLarger(height: .Inches_4_7)
    }
    
    public static func IS_4_7_INCHES_OR_SMALLER() -> Bool {
        return isPhone() && isSizeOrLarger(height: .Inches_4_7)
    }
    
    // MARK: 5.5 Inch Checks
    
    public static func IS_5_5_INCHES() -> Bool {
        return isPhone() && isSize(height: .Inches_5_5)
    }
    
    public static func IS_5_5_INCHES_OR_LARGER() -> Bool {
        return isPhone() && isSizeOrLarger(height: .Inches_5_5)
    }
    
    public static func IS_5_5_INCHES_OR_SMALLER() -> Bool {
        return isPhone() && isSizeOrLarger(height: .Inches_5_5)
    }
    
    // MARK: JailBreak
    public static func isJailBreak() -> Bool {
        let manager = FileManager.default
        #if !(TARGET_IPHONE_SIMULATOR)
            if manager.fileExists(atPath: "/Applications/Cydia.app") ||
                manager.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib") ||
                manager.fileExists(atPath: "/bin/bash") ||
                manager.fileExists(atPath: "/usr/sbin/sshd") ||
                manager.fileExists(atPath: "/etc/apt") ||
                manager.fileExists(atPath: "/private/var/lib/apt/") ||
                UIApplication.shared.canOpenURL(NSURL(string: "cydia://package/com.example.package")! as URL){
                return true
            }
            
            do {
                let stringToBeWritten = "This is a test."
                try stringToBeWritten.write(toFile: "/private/jailbreak.txt", atomically: true, encoding: String.Encoding.utf8)
                try manager.removeItem(atPath: "/private/jailbreak.txt")
                
                return true
            } catch _{
                print("Não conseguiu escrever")
            }
            
        #endif
        
        return false;
    }
}

