//
//  Erros.swift
//  EasyDriver
//
//  Created by Thiago Santos on 19/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation

enum Erros {
    // MARK: Erros comuns
    case FIRAuthErrorCodeNetworkError
    case FIRAuthErrorCodeUserNotFound
    case FIRAuthErrorCodeUserTokenExpired
    case FIRAuthErrorCodeTooManyRequests
    case FIRAuthErrorCodeInvalidAPIKey
    case FIRAuthErrorCodeAppNotAuthorized
    case FIRAuthErrorCodeKeychainError
    case FIRAuthErrorCodeInternalError
    
    // MARK: Erros FIRAuth
    case FIRAuthErrorCodeInvalidEmail
    case FIRAuthErrorCodeOperationNotAllowed
    case FIRAuthErrorCodeUserDisabled
    case FIRAuthErrorCodeWrongPassword
    case FIRAuthErrorCodeInvalidCredential
    case FIRAuthErrorCodeEmailAlreadyInUse
    case FIRAuthErrorCodeInvalidCustomToken
    case FIRAuthErrorCodeCustomTokenMismatch
    
    
    
    
}
