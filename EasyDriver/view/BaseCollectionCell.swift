//
//  BaseCollectionCell.swift
//  EasyDriver
//
//  Created by Thiago Santos on 12/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class BaseCollectionCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
        
    }
    
    
    
    func setupViews() {
        
    }
}
