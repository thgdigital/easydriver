//
//  HistoricoCell.swift
//  EasyDriver
//
//  Created by Thiago Santos on 10/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import SDWebImage

class HistoricoCell: UITableViewCell {

    @IBOutlet weak var avatarImageViw: UIImageView!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var reaisLabel: UILabel!
    
    
    var historico : Historico?{
        didSet {
            if let valor = historico?.valor{
                reaisLabel.text = valor
            }
            if let motorista = historico?.motorista?.nome {
                subTitleLabel.text = motorista
            }
            if let image = historico?.motorista?.image {
                avatarImageViw.sd_setImage(with: URL(string: image))
            }
            if let data = historico?.date_corrida{
                tituloLabel.text = data
            }
            
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        avatarImageViw?.layer.cornerRadius = avatarImageViw.frame.height / 2
        avatarImageViw?.contentMode = .scaleAspectFill
        avatarImageViw?.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
