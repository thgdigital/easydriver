//
//  FavoritoCell.swift
//  EasyDriver
//
//  Created by Thiago Santos on 11/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class FavoritoCell: UITableViewCell {
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    var favorito: Favorito?{
        didSet{
            if let avatar = favorito?.motorista?.image{
                avatarImageView.sd_setImage(with: URL(string: avatar))
            }
            if let name = favorito?.motorista?.nome{
                tituloLabel.text = name
            }
            if let carro = favorito?.motorista?.carro{
                subTitleLabel.text = carro
            }
            if let status = favorito?.status{
                statusLabel.text = status.uppercased()
                statusLabel.textColor = configStatus(string: status)
            }
        }
    }

    private func configStatus(string: String) -> UIColor {
        if string == "Disponivel"{
            return UIColor.rgb(red: 46, green: 125, blue: 50)
        }else if string == "OffLine" {
            return UIColor.red
        }else{
            return UIColor.gray
        }
    
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        avatarImageView?.layer.cornerRadius = avatarImageView.frame.height / 2
        avatarImageView?.contentMode = .scaleAspectFill
        avatarImageView?.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
