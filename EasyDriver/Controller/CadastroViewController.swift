//
//  CadastroViewController.swift
//  EasyDriver
//
//  Created by Thiago Santos on 24/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import Firebase

class CadastroViewController: BasicViewController {
    
    
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CadastroViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        setupLayout()
    }
     override func setupLayout(){
        super.setupLayout()
        
        emailTextField.delegate = self
        fullNameTextField.delegate = self
        passwordTextField.delegate = self
        sendButton.addTarget(self, action: #selector(CadastroViewController.sendCadastro), for: .touchUpInside)
        avatarImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CadastroViewController.modificarAvatar)))
        avatarImageView.layer.cornerRadius =  avatarImageView.frame.width / 2
        avatarImageView.contentMode = .scaleAspectFill
        avatarImageView.clipsToBounds = true
        
        closeButton.setImage(UIImage(named: "close")?.maskWithColor(color: .white), for: .normal)
        closeButton.addTarget(self, action: #selector(CadastroViewController.closeController), for: .touchUpInside)
    }
    
   
    func sendCadastro(){
        
        self.activityIndicatorView.startAnimating()
        
        guard let name = fullNameTextField.text, let email = emailTextField.text, let passorwd = passwordTextField.text,Utils.isValidEmail(email: email) else {
            self.activityIndicatorView.stopAnimating()
            UIAlertController.showAlert(title: "Opss Error", message: "Verificar se exitem dados em brancos ou e-mail valido.", cancelButtonTitle: "Fechar",  cancelBlock: nil)
            return
        }
        Auth.auth().createUser(withEmail: email, password: passorwd) { (fireuser, error) in
            if  error != nil{
                 self.activityIndicatorView.stopAnimating()
                UIAlertController.showAlert(title: "Opss Error", message: (error?.localizedDescription)!, cancelButtonTitle: "Fechar",  cancelBlock: nil)
                
                return
            }
            if let user = fireuser {
                let user = User(uid: user.uid, fullName: name, profileImage: self.avatarImageView.image, email: email, userName: "")
                user.save { (error) in
                    guard let _ = error else{
                        
                         self.activityIndicatorView.stopAnimating()
                          UIAlertController.showAlert(title: "Opss Error", message:  (error?.localizedDescription)!, cancelButtonTitle: "Fechar",  cancelBlock: nil)
                        return
                    }
                    
                    Auth.auth().signIn(withEmail: email, password: passorwd, completion: { (fireUser, error) in
                        
                        guard let _ = error else{
                             self.activityIndicatorView.stopAnimating()
                            UIAlertController.showAlert(title: "Opss Error", message:  (error?.localizedDescription)!, cancelButtonTitle: "Fechar",  cancelBlock: nil)
                            return
                        }
                        self.activityIndicatorView.stopAnimating()
                       self.dismiss(animated: true, completion: nil)
                    })
                }
            
            }
           
        }
        

    }
    func closeController(){
     dismiss(animated: true, completion: nil)
    }
   
    

    // MARK: - Instance
    
    class func instantiateFromStoryboard() -> CadastroViewController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "CadastroViewController") as! CadastroViewController
    }
    
  
}


// MARK: - Extension UIImagePickerControllerDelegate
extension CadastroViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func modificarAvatar(){
        let piker = UIImagePickerController()
        piker.delegate = self
        piker.allowsEditing = true
        present(piker, animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("cancelar")
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
            self.avatarImageView.image = editedImage
        }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
         self.avatarImageView.image = originalImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
}
