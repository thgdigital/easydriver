//
//  LoginController.swift
//  EasyDriver
//
//  Created by Thiago Santos on 19/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import Firebase

class LoginController: BasicViewController {
    
    
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var cadastroButton: UIButton!
    @IBOutlet weak var topLogoConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var boxFormalarioView: UIView!
    
    // MARK: - Instance
    
    class func instantiateFromStoryboard() -> LoginController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "LoginController") as! LoginController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }

    // MARK: - Layout
     override func setupLayout(){
        super.setupLayout()
       boxFormalarioView.layer.cornerRadius = 5
        loginButton.layer.cornerRadius = 5
        cadastroButton.layer.cornerRadius = 5
        passwordTextField.delegate = self
        emailTextField.delegate = self
        
        loginButton.addTarget(self, action: #selector(LoginController.login), for: .touchUpInside)
        
        cadastroButton.addTarget(self, action: #selector(LoginController.cadastro), for: .touchUpInside)
        
      
        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

 
    }
    
    // MARK: - Login
    
    @objc private func login(){
        activityIndicatorView.startAnimating()
        guard let email = emailTextField.text ,  let password = passwordTextField.text, Utils.isValidEmail(email: email) else {
            UIAlertController.showAlert(title: "Opss Error", message: "Verificar se exitem dados em brancos ou e-mail valido.", cancelButtonTitle: "Fechar",  cancelBlock: nil)
            activityIndicatorView.stopAnimating()
            return
        
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            
            if error != nil {
                
                
                if let errCode = AuthErrorCode(rawValue: error!._code) {
                    switch errCode {
                        case .invalidEmail:
                          UIAlertController.showAlert(title: "Opss Error", message: "E-mail invalido", cancelButtonTitle: "Fechar", cancelBlock: nil)
                        case .emailAlreadyInUse:
                              UIAlertController.showAlert(title: "Opss Error", message: "Conta em uso", cancelButtonTitle: "Fechar", cancelBlock: nil)
                        
                        case .userNotFound:
                             UIAlertController.showAlert(title: "Opss Error", message: "Conta não encontrada", cancelButtonTitle: "Fechar", cancelBlock: nil)
                        case .userDisabled:
                            UIAlertController.showAlert(title: "Opss Error", message: "Conta desativada", cancelButtonTitle: "Fechar", cancelBlock: nil)
                        
                        default:
                            print("Create User Error: \(error!)")
                        }
                    
                    }
                
                }
                self.activityIndicatorView.stopAnimating()
            }
        
        }

     // MARK: - Cadastro
    func cadastro(){
        let cadastro = CadastroViewController.instantiateFromStoryboard()
        present(cadastro, animated: true, completion: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if Device.IS_4_INCHES_OR_SMALLER(){
                if topLogoConstraint.constant == 60{
                    topLogoConstraint.constant -=  keyboardSize.height - 100
            
                    UIView.animate(withDuration: 0, delay: 0, options: .curveEaseInOut, animations: {
                        self.view.layoutIfNeeded()
                    
                    }, completion: { (finishid) in
                    
                    })
                }
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if let _ = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if Device.IS_4_INCHES_OR_SMALLER(){
                topLogoConstraint.constant = 60
                UIView.animate(withDuration: 0, delay: 0, options: .curveEaseInOut, animations: {
                    self.view.layoutIfNeeded()
                
                }, completion: { (finishid) in
                
                })
            }
        }
    }

}
