//
//  ProfileTableViewController.swift
//  EasyDriver
//
//  Created by Thiago Santos on 18/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import Firebase

class ProfileTableViewController: UITableViewController {

    @IBOutlet weak var profileImageview: UIImageView!
    
    @IBAction func logout(_ sender: Any) {
        do {
            try Auth.auth().signOut()
        } catch let logoutError {
            print(logoutError)
        }
        
    }
    @IBOutlet weak var logoutButtonItem: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProfile()
    }

    private func setupProfile(){
        
        titleLabel(string: "Editar Conta")
        
        self.tableView.tableFooterView = UIView()
        profileImageview.layer.cornerRadius = profileImageview.frame.height / 2
        profileImageview.contentMode = .scaleAspectFill
        profileImageview.clipsToBounds = true
        
    }
    

}
