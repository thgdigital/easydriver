//
//  BasicViewController.swift
//  EasyDriver
//
//  Created by Thiago Santos on 26/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import Firebase

class BasicViewController: UIViewController {
    
    let activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        aiv.hidesWhenStopped = true
        aiv.color = .black
        aiv.stopAnimating()
        return aiv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupLayout()
    }

    // MARK: - Layout
     func setupLayout(){
        guard  Auth.auth().currentUser != nil else{
            // User is signed in.
            if let window = UIApplication.shared.keyWindow {
                window.rootViewController = CustomTabBarController.instantiateFromStoryboard()
            }
            return
        }
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BasicViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        view.addSubview(activityIndicatorView)
        activityIndicatorView.anchorCenterXToSuperview()
        activityIndicatorView.anchorCenterYToSuperview()
    }

    func dismissKeyboard() {
        view.endEditing(true)
    }

  

}
// MARK: - Extension UITextFieldDelegate
extension BasicViewController:  UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        textField.resignFirstResponder()
        return true
    }
}

