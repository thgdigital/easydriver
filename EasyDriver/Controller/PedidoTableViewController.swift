//
//  PedidoTableViewController.swift
//  EasyDriver
//
//  Created by Thiago Santos on 11/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class PedidoTableViewController: UITableViewController {

    @IBOutlet weak var pedidoButton: UIButton!
    
    var pedidos = [Pedido]()
    
    // MARK: - Instance
    
    class func instantiateFromStoryboard() -> PedidoTableViewController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "PedidoTableViewController") as! PedidoTableViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
         titleLabel(string: "Meus Pedidos")

        let img = UIImage(named: "magnify")!.maskWithColor(color: UIColor.white)
       
        let imgWidth = img.size.width
        let imgHeight = img.size.height
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: imgWidth, height: imgHeight))
        button.setBackgroundImage(img, for: .normal)
        button.addTarget(self, action: #selector(search), for: UIControlEvents.touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        
        pedidoButton.layer.cornerRadius = 5
      
    }
    
    func search(){
    
    }
    @IBAction func nemPedido(_ sender: Any) {
        
        let mapViewController = MapViewController.instantiateFromStoryboard()
         self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(mapViewController, animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    Api.sharedInstance.getPedidos(url: "http://private-a0fba-easydrive.apiary-mock.com/pedidos") { (pedidos, error) in
        
        if error != nil {
            // TODO: Tratar o error
            print(error!)
            return
        }
        
        self.pedidos = pedidos
        self.tableView.reloadData()
        }
  
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return pedidos.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pedidoCell", for: indexPath) as! PedidoCell

        // Configure the cell...
        cell.pedido = pedidos[indexPath.row]
        return cell
    }


}
